Enexis
======

Import [Enexis open data](https://www.enexis.nl/over-ons/wat-bieden-we/andere-diensten/open-data).

# Overview

This program reads in one or more CSV files and converts them to a CIM file.

The program runs on [Spark](https://spark.apache.org), usually on a cluster such as
[Amazon Web Services (AWS)](https://aws.amazon.com), [Azure](https://docs.microsoft.com/en-us/azure) or
an in-house system.

# Input

The input is one or more CSV format file (with a header and separator';') described by the meta data in the 'Metadata Open Asset Data Attributen Elektra.xlsx' file.

Optionally, one can include library (header files of CIM elements) to be included in the output file.

# Output

The output file (with extension .rdf) is produced in the same directory as the input file.
