package ch.ninecode.enexis

import java.sql.Timestamp
import java.text.SimpleDateFormat

import scala.collection._

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import ch.ninecode.cim.CIMExport
import ch.ninecode.model._

/**
 * Import measured data into Cassandra.
 *
 * Copies files to HDFS, reads them into Spark, executes a join across a CH### to mRID mapping table and stores them in Cassandra.
 *
 * @param session The Spark session to use.
 * @param options Options regarding Cassandra master, files to process etc.
 */
case class Enexis (session: SparkSession, options: EnexisOptions)
{
    type Point = (Double, Double)

    if (options.verbose)
        org.apache.log4j.LogManager.getLogger (getClass).setLevel (org.apache.log4j.Level.INFO)
    val log: Logger = LoggerFactory.getLogger (getClass)

    val storage: StorageLevel = StorageLevel.fromString (options.storage)
    val factory: ElementFactory = ElementFactory ()

    def csv_options: mutable.HashMap[String, String] =
    {
        val options = new mutable.HashMap[String, String]()

        val header = "true"
        val ignoreLeadingWhiteSpace = "false"
        val ignoreTrailingWhiteSpace = "false"
        val sep = ";"
        val quote = "\""
        val escape = "\\"
        val encoding = "UTF-8"
        val comment = "#"
        val nullValue = ""
        val nanValue = "NaN"
        val positiveInf = "Inf"
        val negativeInf = "-Inf"
        val dateFormat = "yyyy-mm-dd"
        val timestampFormat = "yyyy.mm.dd HH:mm:ss"
        val mode = "DROPMALFORMED"
        val inferSchema = "true"

        options.put ("header", header)
        options.put ("ignoreLeadingWhiteSpace", ignoreLeadingWhiteSpace)
        options.put ("ignoreTrailingWhiteSpace", ignoreTrailingWhiteSpace)
        options.put ("sep", sep)
        options.put ("quote", quote)
        options.put ("escape", escape)
        options.put ("encoding", encoding)
        options.put ("comment", comment)
        options.put ("nullValue", nullValue)
        options.put ("nanValue", nanValue)
        options.put ("positiveInf", positiveInf)
        options.put ("negativeInf", negativeInf)
        options.put ("dateFormat", dateFormat)
        options.put ("timestampFormat", timestampFormat)
        options.put ("mode", mode)
        options.put ("inferSchema", inferSchema)

        options
    }

    def extract_points (wkt: String): List[Point] =
    {
        val coordinates = wkt.substring ("LINESTRING (".length, wkt.length - 1)
        coordinates.split (",")
            .map (
                pair ⇒
                {
                    val lat_lon = pair.split (" ")
                    val y = lat_lon (0).toDouble
                    val x = lat_lon (1).toDouble
                    (x, y)
                }
            ).toList

    }

    def extract_point (wkt: String): Point =
    {
        val coordinates = wkt.substring ("POINT (".length, wkt.length - 1)
        val lat_lon = coordinates.split (" ")
        val y = lat_lon (0).toDouble
        val x = lat_lon (1).toDouble
        (x, y)
    }

    def level (network: String): String =
    {
        network match
        {
            case "laagspanning" ⇒ "N7"
            case "middenspanning" ⇒ "N5"
            case _ ⇒ "N0"
        }
    }

    def root_name (name: String): String =
    {
        val n = name.replace ('!', '_')
        if (n.startsWith ("_elec_"))
            n.substring ("_elec_".length)
        else
            n
    }

    def root_number (name: String): String =
    {
        val n = name.replace ('!', '_')
        n.substring (n.lastIndexOf ('_') + 1)
    }

    def cable_name (name: String): String =
    {
        "_cable_" + root_name (name).replace ("_cable", "")
    }

    def terminal_name (name: String, number: Int): String =
    {
        "_terminal_" + number + cable_name (name)
    }

    def location_name (name: String): String =
    {
        "_location_" + root_name (name)
    }

    def service_location_name (name: String): String =
    {
        "_user_" + root_name (name)
    }

    def station_name (name: String): String =
    {
        "_station_" + root_name (name)
    }

    def terminals (name: String, parent: String, network: String): List[Element] =
    {
        val mrid1 = terminal_name (name, 1)
        val mrid2 = terminal_name (name, 2)
        val number = root_number (name)

        val obj1 = factory.identified_object (mrid1, number, s"terminal 1 for ${level (network)} cable $number")
        val terminal1 = factory.terminal (obj1, 1, parent)

        val obj2 = factory.identified_object (mrid2, number, s"terminal 2 for ${level (network)} cable $number")
        val terminal2 = factory.terminal (obj2, 2, parent)

        terminal1 :: terminal2 :: Nil
    }

    def a_point (base_name: String, location_name: String, point: Point): Element =
    {
        val x = point._1
        val y = point._2
        val mrid = s"_location${base_name}_p"
        val pp = new PositionPoint (sup = factory.basic (mrid), groupNumber = 1, sequenceNumber = 1, xPosition = x.toString, yPosition = y.toString, Location = location_name)
        pp.bitfields = factory.setFields[PositionPoint] (Array ("groupNumber", "sequenceNumber", "xPosition", "yPosition", "Location"))
        pp
    }

    def point_sequence (base_name: String, location_name: String, points: List[Point]): List[Element] =
    {
        points.zipWithIndex.map (
            coordinates ⇒
            {
                val n = coordinates._2 + 1
                val x = coordinates._1._1
                val y = coordinates._1._2
                val mrid = s"_location${base_name}_p$n"
                val element = factory.basic (mrid)
                val pp = new PositionPoint (element, groupNumber = 1, sequenceNumber = n, xPosition = x.toString, yPosition = y.toString, Location = location_name)
                pp.bitfields = factory.setFields[PositionPoint] (Array ("groupNumber", "sequenceNumber", "xPosition", "yPosition", "Location"))
                pp
            }
        )
    }

    def location (name: String, parent: String): Element =
    {
        val mrid = location_name (name)
        val number = root_number (name)

        val obj = factory.identified_object (mRID = mrid, name = number, description = s"location of $parent")
        val location = new Location (sup = obj, `type` = "geographical", CoordinateSystem = "wgs84")
        location.bitfields = factory.setFields[Location] (Array ("geographical", "wgs84"))
        location
    }

    lazy val date_format = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss z")

    def assets (name: String, parent: String, network: String, installed: Timestamp, updated: Timestamp, `type`: String): List[Element] =
    {
        val location_mrid = location_name (name)
        val number = root_number (name)

        val lifecycle_mrid = s"_lifecycle$parent"
        val lifecycle_element = factory.basic (lifecycle_mrid)
        val lifecycle = LifecycleDate (sup = lifecycle_element, installationDate = date_format.format (installed))
        lifecycle.bitfields = factory.setFields[LifecycleDate] ("installationDate")

        val status_mrid = s"_status$parent"
        val status_element = factory.basic (status_mrid)
        val status = Status (sup = status_element, dateTime = date_format.format (updated), value = "updated")
        status.bitfields = factory.setFields[Status] (Array ("dateTime", "value"))

        val asset_mrid = s"_asset$parent"
        val asset_obj = factory.identified_object (mRID = asset_mrid, name = number, description = s"asset record for $parent")
        val asset = Asset (sup = asset_obj, lifecycleDate = lifecycle.id, status = status.id, `type` = `type`, Location = location_mrid, PowerSystemResources = List(parent))
        asset.bitfields = factory.setFields[Asset] (Array ("lifecycleDate", "status", "type", "Location", "PowerSystemResources"))

        lifecycle :: status :: asset :: Nil
    }

    def generate_cable_from_length (length: Double): String =
    {
        if (length < 20.0)
            "GKN 3x10re/10 1/0.6 kV"
        else if (length < 40.0)
            "GKN 3x16rm/16 1/0.6 kV"
        else if (length < 75.0)
            "GKN 3x25rm/25 1/0.6 kV"
        else if (length < 100.0)
            "GKN 3x50rm/50 1/0.6 kV"
        else if (length < 200.0)
            "GKN 3x95se/95 1/0.6 kV"
        else if (length < 400.0)
            "GKN 3x150se/150 1/0.6 kV"
        else
            "GKN 3x240se/240 1/0.6 kV"
    }

    def cable_datasheet (name: String): (String, String) =
    {
        name match
        {
            case "GKN 3x10re/10 1/0.6 kV" ⇒ ("_c246ef3a-3015-4e8e-9fa6-334b633e27fb", "_1880ab2b-813a-4a44-b5c0-18cb5d870b59")
            case "GKN 3x16rm/16 1/0.6 kV" ⇒ ("_fbd62bd8-049f-4b0c-a372-9ee334d509b8", "_b95f1837-5bdc-4878-89ce-d6c7a37b8f12")
            case "GKN 3x25rm/25 1/0.6 kV" ⇒ ("_8c498939-5f85-4d48-adc7-0147aa30f19c", "_20480b94-e981-4a76-9c88-4aef3d1b8be2")
            case "GKN 3x50rm/50 1/0.6 kV" ⇒ ("_a62be941-4f47-48cf-a103-c69b7bd928b0", "_e6146ff6-4bd4-4c69-99f0-915e87a482bf")
            case "GKN 3x95se/95 1/0.6 kV" ⇒ ("_a5a4a4fc-997c-459c-a6d5-34a409308627", "_85d0b44b-7e9a-4520-b87a-1981e3cef73a")
            case "GKN 3x150se/150 1/0.6 kV" ⇒ ("_27041c0f-4097-40a0-a342-4775036c0fa8", "_b820b617-2d30-490c-b85d-ce2bcd172f39")
            case "GKN 3x240se/240 1/0.6 kV" ⇒ ("_1d65b0d8-ac40-4d53-a5ba-39de8da7c0b3", "_d8503de3-4c1c-402b-9633-8a8f542cb9ce")
            case _ ⇒ ("_c246ef3a-3015-4e8e-9fa6-334b633e27fb", "_1880ab2b-813a-4a44-b5c0-18cb5d870b59")
        }
    }

    def cable_psrtype (laying: String): String =
    {
        laying match
        {
            case "underground" ⇒ "PSRType_Underground"
            case _ ⇒ "PSRType_Unknown"
        }
    }

    def extract_container (label: String): String =
    {
        if (label.startsWith ("Kabelgroep: "))
            label.substring ("Kabelgroep: ".length)
        else if (label.startsWith ("Tracenaam: "))
            label.substring ("Tracenaam: ".length)
        else
            null
    }

    def base_voltage (volts: Double): String =
    {
        volts match
        {
            case 0.0 ⇒ "BaseVoltage_Unknown"
            case 230.0 ⇒ "BaseVoltage_230"
            case 400.0 ⇒ "BaseVoltage_400"
            case 10000.0 ⇒ "BaseVoltage_10000"
            case 50000.0 ⇒ "BaseVoltage_50000"
            case _ ⇒ "BaseVoltage_Unknown"
        }
    }

    def service_status (status: String): String =
    {
        status match
        {
            case "functional" ⇒ "in_use"
            case "disused" ⇒ "not_in_use"
            case "projected" ⇒ "planned"
            case _ ⇒ "unknown"
        }
    }

    def distance (p1: Point, p2: Point, r: Double = 6378.137e3 /* average radius of the earth in m */): Double =
    {
        def deg2rad (degrees: Double): Double = degrees * Math.PI / 180.0

        val lon1 = p1._1
        val lat1 = p1._2
        val lon2 = p2._1
        val lat2 = p2._2
        val delta_lat = deg2rad (lat2 - lat1)
        val delta_lon = deg2rad (lon2 - lon1)
        val haversindlat = Math.sin (delta_lat / 2.0)
        val haversindlon = Math.sin (delta_lon / 2.0)
        val haversin = haversindlat * haversindlat + Math.cos (deg2rad (lat1)) * Math.cos (deg2rad (lat2)) * haversindlon * haversindlon
        val centralangle = 2.0 * Math.atan2 (Math.sqrt (haversin), Math.sqrt (1.0 - haversin))
        r * centralangle
    }

    def path_length (points: List[Point]): Double =
    {
        points.toIterator.sliding (2).toList.map (x ⇒ distance (x.head, x.tail.head)).sum
    }

    def cable (name: String, network: String, psrtype: String, datasheet: String, container: String, voltage: String, status: String, length: Double, perlengthimpedance: String): ACLineSegment =
    {
        val mrid = cable_name (name)
        val location_mrid = location_name (name)
        val number = root_number (name)

        val obj = factory.identified_object (mrid, number, level (network) + " cable " + number, name)
        val psr = PowerSystemResource (sup = obj, AssetDatasheet = datasheet, Location = location_mrid, PSRType = psrtype)
        psr.bitfields = factory.setFields[PowerSystemResource](Array ("AssetDatasheet", "Location", "Location"))
        val equipment = Equipment (sup = psr, inService = status == "in_use", EquipmentContainer = container)
        equipment.bitfields = factory.setFields[Equipment](Array ("inService", "EquipmentContainer"))
        val conducting_equipment = ConductingEquipment (sup = equipment, BaseVoltage = voltage)
        conducting_equipment.bitfields = factory.setFields[ConductingEquipment]("BaseVoltage")
        val conductor = Conductor (sup = conducting_equipment, len = length)
        conductor.bitfields = factory.setFields[Conductor]("length")
        val cable = ACLineSegment (sup = conductor, PerLengthImpedance = perlengthimpedance)
        cable.bitfields = factory.setFields[ACLineSegment]("PerLengthImpedance")
        cable
    }

    def line (name: String, parent: String, psrtype: String, region: String): Line =
    {
        val mrid = name
        val number = root_number (name)

        val obj = factory.identified_object (mrid, number,"container for " + parent)
        val psr = PowerSystemResource (sup = obj, PSRType = psrtype)
        psr.bitfields = factory.setFields[PowerSystemResource]("PSRType")
        val ss = ConnectivityNodeContainer (psr)
        ss.bitfields = Array (0)
        val container = EquipmentContainer (ss)
        container.bitfields = Array (0)
        val line = Line (container, Region = region)
        line.bitfields = factory.setFields[Line]("Region")
        line
    }

    def close (p1: Point, p2: Point): Boolean =
    {
        val dx = p1._1 - p2._1
        val dy = p1._2 - p2._2
        math.sqrt (dx * dx + dy*dy) < 1e-9
    }

    def tail_head (array: Array[List[Point]]): Array[List[Point]] =
    {
        for (i ← array.indices)
            for (j ← array.indices)
                if (i != j && close (array(i).last, array(j).head))
                {
                    val new_array = new Array[List[Point]](array.length - 1)
                    new_array(0) = array(i) ::: array(j).tail
                    var index = 1
                    for (k ← array.indices)
                        if ((k != i) && (k != j))
                        {
                            new_array(index) = array(k)
                            index = index + 1
                        }
                    return new_array
                }
        null
    }

    def head_tail (array: Array[List[Point]]): Array[List[Point]] =
    {
        for (i ← array.indices)
            for (j ← array.indices)
                if (i != j && close (array(i).head, array(j).last))
                {
                    val new_array = new Array[List[Point]](array.length - 1)
                    new_array(0) = array(j) ::: array(i).tail
                    var index = 1
                    for (k ← array.indices)
                        if ((k != i) && (k != j))
                        {
                            new_array(index) = array(k)
                            index = index + 1
                        }
                    return new_array
                }
        null
    }

    def tail_tail (array: Array[List[Point]]): Array[List[Point]] =
    {
        for (i ← array.indices)
            for (j ← array.indices)
                if (i != j && close (array(i).last, array(j).last))
                {
                    val new_array = new Array[List[Point]](array.length - 1)
                    new_array(0) = array(i) ::: array(j).reverse.tail
                    var index = 1
                    for (k ← array.indices)
                        if ((k != i) && (k != j))
                        {
                            new_array(index) = array(k)
                            index = index + 1
                        }
                    return new_array
                }
        null
    }

    def head_head (array: Array[List[Point]]): Array[List[Point]] =
    {
        for (i ← array.indices)
            for (j ← array.indices)
                if (i != j && close (array(i).head, array(j).head))
                {
                    val new_array = new Array[List[Point]](array.length - 1)
                    new_array(0) = array(i).reverse ::: array(j).tail
                    var index = 1
                    for (k ← array.indices)
                        if ((k != i) && (k != j))
                        {
                            new_array(index) = array(k)
                            index = index + 1
                        }
                    return new_array
                }
        null
    }

    def join (iter: Iterable[List[Point]]): List[Point] =
    {
        var array = iter.toArray
        var changed = false
        do
        {
            changed = false

            // check for tail of one matching the head of another
            val a = tail_head (array)
            if (null != a)
            {
                array = a
                changed = true
            }
            else
            {
                val b = head_tail (array)
                if (null != b)
                {
                    array = b
                    changed = true
                }
                else
                {
                    val c = tail_tail (array)
                    if (null != c)
                    {
                        array = c
                        changed = true
                    }
                    else
                    {
                        val d = head_head (array)
                        if (null != d)
                        {
                            array = d
                            changed = true
                        }
                    }
                }
            }

        }
        while (changed)
        array(0)
    }

    case class Cable (
        wkt: String,
        installed: Timestamp,
        name: String,
        network: String,
        status: String,
        updated: Timestamp,
        laying: String,
        voltage: Double,
        label: String
        )

    def sub_cable (filename: String, region: String): RDD[Element] =
    {
        log.info ("reading %s".format (filename))
        val df = session.sqlContext.read.format ("csv").options (csv_options).csv (filename)
        val rdd = df.rdd.map (row ⇒
            Cable (
                row.getString (0),
                row.getTimestamp (1),
                row.getString (2),
                row.getString (3),
                row.getString (5),
                row.getTimestamp (6),
                row.getString (7),
                row.getDouble (12),
                row.getString (14)
                )).cache
        val chunks = rdd.groupBy (_.name).values
        chunks.flatMap (
            chunk ⇒
            {
                val master = chunk.head
                val points = if (chunk.size > 1)
                {
                    val all_points: Iterable[List[Point]] = chunk.map (_.wkt).map (extract_points)
                    join (all_points)
                }
                else
                    extract_points (chunk.head.wkt)
                // about 1.5GB: p ⇒ p._2 > 53.28
                // about 500MB: p ⇒ p._2 > 53.35
                // about 1MB: p ⇒ p._1 < 6.1845 && p._2 > 52.907
                if (!options.filter || points.exists (p ⇒ p._2 > 53.35))
                {
                    val name = master.name
                    val network = master.network
                    val length = path_length (points)
                    val psrtype = cable_psrtype (master.laying)
                    val `type` = generate_cable_from_length (length)
                    val (datasheet, perlengthimpedance) = cable_datasheet (`type`)
                    val group = extract_container (master.label)
                    val l: Option[Line] = if (null != group)
                        Some(line (group, cable_name (name), psrtype, region))
                    else
                        None
                    val voltage = base_voltage (master.voltage)
                    val status = service_status (master.status)

                    cable (name, network, psrtype, datasheet, group, voltage, status, length, perlengthimpedance) ::
                    location (name, network) ::
                    terminals (name, cable_name (name), network) :::
                    point_sequence (root_name (name), location_name (name), points) :::
                    assets (name, cable_name (name), network, master.installed, master.updated, `type`) ++ l
                }
                else
                    List ()
            }
        )
    }

    def vertical_type (vertical: String): String =
    {
        vertical match
        {
            case "onGroundSurface" ⇒ "on surface"
            case "suspendedOrElevated" ⇒ "elevated"
        }
    }

    def service_connection (name: String, psrtype: String, vertical: String, status: String): EnergyConsumer =
    {
        val mrid = service_location_name (name)
        val location = location_name (name)
        val number = root_number (name)

        val obj = factory.identified_object (mrid, number, "energy consumer " + number + " " + vertical, name)
        val psr = PowerSystemResource (sup = obj, Location = location, PSRType = psrtype)
        psr.bitfields = factory.setFields[PowerSystemResource](Array ("Location", "PSRType"))
        val equipment = Equipment (sup = psr, normallyInService = status == "in_use")
        equipment.bitfields = factory.setFields[Equipment]("normallyInService")
        val conducting_equipment = ConductingEquipment (sup = equipment)
        conducting_equipment.bitfields = Array(0)
        val connection = EnergyConnection (sup = conducting_equipment)
        connection.bitfields = Array(0)
        val customerCount = psrtype match
        {
            case "PSRType_HouseService" ⇒ 1
            case "PSRType_MultiStoryService" ⇒ 3
            case _ ⇒ 0
        }
        val consumer = EnergyConsumer (sup = connection, customerCount = customerCount)
        consumer.bitfields =  factory.setFields[EnergyConsumer]("customerCount")
        consumer
    }

    case class ServiceConnection (
         wkt: String,
         installed: Timestamp,
         name: String,
         status: String,
         updated: Timestamp,
         vertical: String)

    def sub_service_connection (filename: String, psrtype: String): RDD[Element] =
    {
        log.info ("reading %s".format (filename))
        val df = session.sqlContext.read.format ("csv").options (csv_options).csv (filename)
        val rdd = df.rdd.map (row ⇒
            ServiceConnection (
                row.getString (0),
                row.getTimestamp (1),
                row.getString (2),
                row.getString (4),
                row.getTimestamp (5),
                row.getString (6))).cache
        rdd.flatMap (
            house ⇒
            {
                val point = extract_point (house.wkt)
                if (!options.filter || point._2 > 53.35)
                {
                    val name = house.name
                    val root = root_name (name)
                    val vertical = vertical_type (house.vertical)
                    val status = service_status (house.status)
                    // voltage, container ??
                    val service = service_connection (name, psrtype, vertical, status)
                    val number = service.EnergyConnection.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.name
                    val loc = location (name, service.id)

                    service ::
                    loc ::
                    a_point (root, loc.id, point) ::
                    factory.terminal (factory.identified_object ("_terminal_" + service.id, number, ""), 1, service.id) ::
                    Nil
                }
                else
                    List ()
            }
        )
    }

    def station_description (description: String): (String, String) =
    {
        description match
        {
            case "Distributieverdeelstation" ⇒ ("distribution station", "PSRType_TransformerStation")
            case "Grondcontactdoos" ⇒ ("ground socket", "PSRType_Vault")
            case "Hoofdstation" ⇒ ("main station", "PSRType_Substation")
            case "Klantstation" ⇒ ("customer station", "PSRType_TransformerStation")
            case "Netstation" ⇒ ("network station", "PSRType_TransformerStation")
            case "Noodcondensatorbank" ⇒ ("emergency capacitor bank", "PSRType_TransformerStation")
            case "Transportverdeelstation" ⇒ ("transport distribution station", "PSRType_CapacitorBank")
            case "Onbekend" ⇒ ("unknown", "PSRType_Unknown")
            case "Flatkast" ⇒ ("flat cabinet", "PSRType_DistributionBox")
            case "LS Nulpuntstransformator" ⇒ ("zero point transformer", "PSRType_DistributionBox")
            case "LS Regeltransformator" ⇒ ("control transformer", "PSRType_DistributionBox")
            case "Ov-voedingpunt" ⇒ ("maintenance point", "PSRType_DistributionBox")
            case "Verdeelkast" ⇒ ("distribution box", "PSRType_DistributionBox")
            case "Aansluitkast" ⇒ ("connection box", "PSRType_ConnectionBox")
            case _ ⇒ ("unknown", "PSRType_Unknown")
        }
    }

    def substation  (name: String, psrtype: String, description: String, vertical: String, status: String, region: String): Substation =
    {
        val mrid = station_name (name)
        val location = location_name (name)
        val number = root_number (name)

        val obj = factory.identified_object (mrid, number, description + " " + number + " " + vertical, name)
        val psr = PowerSystemResource (sup = obj, Location = location, PSRType = psrtype)
        psr.bitfields = factory.setFields[PowerSystemResource] (Array("Location", "PSRType"))
        val ss = ConnectivityNodeContainer (psr)
        ss.bitfields = Array (0)
        val container = EquipmentContainer (ss)
        container.bitfields = Array (0)
        val station = Substation (container, Region = region)
        station.bitfields = factory.setFields[PowerSystemResource] ("Region")
        station
    }

    case class Station (
         wkt: String,
         status: String,
         updated: Timestamp,
         vertical: String,
         description: String,
         name: String
    )

    def sub_station (filename: String, is_distribution: Boolean = false): RDD[Element] =
    {
        log.info ("reading %s".format (filename))
        val df = session.sqlContext.read.format ("csv").options (csv_options).csv (filename)
        val rdd = df.rdd.map (row ⇒
            Station (
                row.getString (0),
                row.getString (1),
                row.getTimestamp (2),
                row.getString (3),
                row.getString (if (is_distribution) 5 else 4),
                row.getString (if (is_distribution) 4 else 5))).cache
        rdd.flatMap (
            station ⇒
            {
                val point = extract_point (station.wkt)
                if (!options.filter || point._2 > 53.35)
                {
                    val name = station.name
                    val root = root_name (name)
                    val (description, psrtype) = station_description (station.description)
                    val vertical = vertical_type (station.vertical)
                    val status = service_status (station.status)
                    val s = substation (name, psrtype, description, vertical, status, "Enexis_North")
                    val loc = location (name, s.id)
                    s ::
                        loc ::
                        a_point (root, loc.id, point) ::
                        Nil
                }
                else
                    List ()
            }
        )
    }

    def process_cable (filename: String): RDD[Element] =
    {
        val start = System.nanoTime ()
        val elements = sub_cable (filename, "Enexis_North").persist (storage)
        log.info ("created %s elements from %s".format (elements.count, filename))
        val end = System.nanoTime ()
        log.info ("process %s: %s seconds".format (filename, (end - start) / 1e9))
        elements
    }

    def process_energy_consumer (psrtype: String) (filename: String): RDD[Element] =
    {
        val start = System.nanoTime ()
        val elements = sub_service_connection (filename, psrtype).persist (storage)
        log.info ("created %s elements from %s".format (elements.count, filename))
        val end = System.nanoTime ()
        log.info ("process %s: %s seconds".format (filename, (end - start) / 1e9))
        elements
    }

    def process_substation (is_distribution: Boolean) (filename: String): RDD[Element] =
    {
        val start = System.nanoTime ()
        val elements = sub_station (filename, is_distribution).persist (storage)
        log.info ("created %s elements from %s".format (elements.count, filename))
        val end = System.nanoTime ()
        log.info ("process %s: %s seconds".format (filename, (end - start) / 1e9))
        elements
    }

    def run (): Unit =
    {
        val begin = System.nanoTime ()
        var ret = if (options.libraries.nonEmpty)
        {
            val start = System.nanoTime
            val cimreader_options = new mutable.HashMap[String, String]()
            cimreader_options.put ("path", options.libraries.mkString (","))
            cimreader_options.put ("StorageLevel", "MEMORY_AND_DISK_SER")
            cimreader_options.put ("ch.ninecode.cim.debug", "true")

            val elements = session.sqlContext.read.format ("ch.ninecode.cim").options (cimreader_options).load (options.libraries: _*).rdd.persist (storage)
            log.info ("%s library elements (%s seconds)".format (elements.count, (System.nanoTime - start) / 1e9))
            val library = session.sparkContext.getPersistentRDDs.filter(_._2.name == "Elements").head._2.asInstanceOf[RDD[Element]]
            library.name = null
            library
        }
        else
            session.sparkContext.emptyRDD[Element]
        if (options.cable_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing cable files: %s".format (options.cable_files.mkString (",")))
            options.cable_files.map (process_cable).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.cable_files.size, if (1 == options.cable_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }
        if (options.service_connection_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing service connection files: %s".format (options.service_connection_files.mkString (",")))
            options.service_connection_files.map (process_energy_consumer ("PSRType_HouseService")).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.service_connection_files.size, if (1 == options.service_connection_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }
        if (options.multistory_connection_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing multi-story connection files: %s".format (options.multistory_connection_files.mkString (",")))
            options.multistory_connection_files.map (process_energy_consumer ("PSRType_MultiStoryService")).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.multistory_connection_files.size, if (1 == options.multistory_connection_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }

        if (options.charging_point_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing charging point files: %s".format (options.charging_point_files.mkString (",")))
            options.charging_point_files.map (process_energy_consumer ("PSRType_ChargingPoint")).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.charging_point_files.size, if (1 == options.charging_point_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }

        if (options.street_furniture_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing street furniture files: %s".format (options.street_furniture_files.mkString (",")))
            options.street_furniture_files.map (process_energy_consumer ("PSRType_SmallConsumer")).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.street_furniture_files.size, if (1 == options.street_furniture_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }

        if (options.street_light_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing street light files: %s".format (options.street_light_files.mkString (",")))
            options.street_light_files.map (process_energy_consumer ("PSRType_StreetLight")).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.street_light_files.size, if (1 == options.street_light_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }

        if (options.station_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing station files: %s".format (options.station_files.mkString (",")))
            options.station_files.map (process_substation (false)).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.station_files.size, if (1 == options.station_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }

        if (options.distribution_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing distribution box files: %s".format (options.distribution_files.mkString (",")))
            options.distribution_files.map (process_substation (true)).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.distribution_files.size, if (1 == options.distribution_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }

        if (options.junction_files.nonEmpty)
        {
            val start = System.nanoTime
            log.info ("processing station files: %s".format (options.junction_files.mkString (",")))
            options.junction_files.map (process_substation (true)).foreach (x ⇒ ret = ret.union (x).persist (storage))
            log.info ("%s file%s (%s seconds)".format (options.junction_files.size, if (1 == options.junction_files.size) "" else "s", (System.nanoTime - start) / 1e9))
        }

        log.info ("processed %s files (%s seconds)".format (options.file_count, (System.nanoTime - begin) / 1e9))

        val start = System.nanoTime
        val export = new CIMExport (session)
        export.export (ret, options.output_file, "Enexis")
        log.info ("saved to %s (%s seconds)".format (options.output_file, (System.nanoTime - start) / 1e9))
    }
}
