package ch.ninecode.enexis

import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.util.Properties

import scala.collection._
import scala.tools.nsc.io.Jar
import scala.util.Random
import scopt.OptionParser

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import ch.ninecode.cim.CIMClasses
import ch.ninecode.cim.CIMExport

object Main
{
    val log: Logger = LoggerFactory.getLogger (getClass)
    val properties: Properties =
    {
        val in = this.getClass.getResourceAsStream ("/application.properties")
        val p = new Properties ()
        p.load (in)
        in.close ()
        p
    }
    val APPLICATION_NAME: String = "Enexis"
    val APPLICATION_VERSION: String = properties.getProperty ("version")
    val SPARK: String = properties.getProperty ("spark")

    object LogLevels extends Enumeration
    {
        type LogLevels = Value
        val ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN = Value
    }

    implicit val LogLevelsRead: scopt.Read[LogLevels.Value] = scopt.Read.reads (LogLevels.withName)

    var do_exit = true

    val parser: OptionParser[EnexisOptions] = new scopt.OptionParser[EnexisOptions](APPLICATION_NAME)
    {
        head (APPLICATION_NAME, APPLICATION_VERSION)

        val default = new EnexisOptions

        override def terminate (exitState: Either[String, Unit]): Unit =
            if (do_exit)
                exitState match
                {
                    case Left (_) => sys.exit (1)
                    case Right (_) => sys.exit (0)
                }

        opt [Unit]("unittest").
            hidden ().
            action ((_, c) ⇒ c.copy (unittest = true)).
            text ("unit testing - don't call sys.exit() [%s]".format (default.unittest))

        opt [Unit]("verbose").
            action ((_, c) ⇒ c.copy (verbose = true)).
            text ("emit progress messages [%s]".format (default.verbose))

        opt [String]("master").valueName ("MASTER_URL").
            action ((x, c) ⇒ c.copy (master = x)).
            text ("local[*], spark://host:port, mesos://host:port or yarn [%s]".format (default.master))

        opt [String]("storage").
            action ((x, c) ⇒ c.copy (storage = x)).
            text ("storage level for RDD serialization [%s]".format (default.storage))

        opt [LogLevels.Value]("logging").
            action ((x, c) ⇒ c.copy (log_level = x)).
            text ("log level, one of " + LogLevels.values.iterator.mkString (",") + " [%s]".format (default.log_level))

        opt [Unit]("filter").
            action ((_, c) ⇒ c.copy (filter = true)).
            text ("apply spatial filter [%s]".format (default.filter))

        opt [Unit]("connect").
            action ((_, c) ⇒ c.copy (connect = true)).
            text ("attempt to create a topology [%s]".format (default.connect))

        opt [String]("output").valueName ("<file>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (output_file = x)).
            text ("output file name [%s]".format (default.output_file))

        opt [String]("library").valueName ("<RDF>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (libraries = c.libraries :+ x)).
            text ("equipment library file (to be prefixed to generated CIM file) [%s]".format (default.libraries.mkString (",")))

        opt [String]("cable").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (cable_files = c.cable_files :+ x)).
            text ("CSV cable file to process [%s]".format (default.cable_files.mkString (",")))

        opt [String]("service").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (service_connection_files = c.service_connection_files :+ x)).
            text ("CSV house connection file to process [%s]".format (default.service_connection_files.mkString (",")))

        opt [String]("multistory").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (multistory_connection_files = c.multistory_connection_files :+ x)).
            text ("CSV multi-story connection file to process [%s]".format (default.multistory_connection_files.mkString (",")))

        opt [String]("charging_point").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (charging_point_files = c.charging_point_files :+ x)).
            text ("CSV charging point file to process [%s]".format (default.charging_point_files.mkString (",")))

        opt [String]("furniture").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (street_furniture_files = c.street_furniture_files :+ x)).
            text ("CSV street furniture file to process [%s]".format (default.street_furniture_files.mkString (",")))

        opt [String]("street_light").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (street_light_files = c.street_light_files :+ x)).
            text ("CSV street light file to process [%s]".format (default.street_light_files.mkString (",")))

        opt [String]("station").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (station_files = c.station_files :+ x)).
            text ("CSV station file to process [%s]".format (default.station_files.mkString (",")))

        opt [String]("distribution").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (distribution_files = c.distribution_files :+ x)).
            text ("CSV distribution box file to process [%s]".format (default.distribution_files.mkString (",")))

        opt [String]("junction").valueName ("<CSV>").optional ().unbounded ().
            action ((x, c) ⇒ c.copy (junction_files = c.junction_files :+ x)).
            text ("CSV junction box file to process [%s]".format (default.junction_files.mkString (",")))

        help ("help").text ("prints this usage text")
    }

    def jarForObject (obj: Object): String =
    {
        // see https://stackoverflow.com/questions/320542/how-to-get-the-path-of-a-running-jar-file
        var ret = obj.getClass.getProtectionDomain.getCodeSource.getLocation.getPath
        try
        {
            ret = URLDecoder.decode (ret, "UTF-8")
        }
        catch
        {
            case e: UnsupportedEncodingException => e.printStackTrace ()
        }
        if (!ret.toLowerCase ().endsWith (".jar"))
        {
            // as an aid to debugging, make jar in tmp and pass that name
            val name = "/tmp/" + Random.nextInt (99999999) + ".jar"
            val writer = new Jar (new scala.reflect.io.File (new java.io.File (name))).jarWriter ()
            writer.addDirectory (new scala.reflect.io.Directory (new java.io.File (ret + "ch/")), "ch/")
            writer.close ()
            ret = name
        }

        ret
    }

    def getSession (options: EnexisOptions, register: Boolean = false): SparkSession =
    {
        // create the configuration
        val configuration = new SparkConf (false)
        configuration.setAppName (APPLICATION_NAME)
        if ("" != options.master)
            configuration.setMaster (options.master)
        if (options.options.nonEmpty)
            options.options.map ((pair: (String, String)) => configuration.set (pair._1, pair._2))
        if (register)
            configuration.registerKryoClasses (CIMClasses.list)

        // get the necessary jar files to send to the cluster
        val s1 = jarForObject (EnexisOptions ())
        val s2 = jarForObject (ch.ninecode.model.BasicElement)
        configuration.setJars (Array (s1, s2))
        configuration.set ("spark.ui.showConsoleProgress", "false")

        // make a Spark session
        SparkSession.builder ().config (configuration).getOrCreate ()
    }

    /**
     * Build jar with dependencies (creates target/program_name_and_version-jar-with-dependencies.jar):
     * mvn package
     * Invoke (on the cluster) with:
     *
     * spark-submit --master spark://sandbox:7077 --driver-memory 2G --executor-memory 4G target/Enexis-2.11-2.3.2-1.0.0-jar-with-dependencies.jar --verbose --library hdfs://sandbox:8020/header.rdf,hdfs://sandbox:8020/equipmentlibrary.rdf hdfs://sandbox:8020/IMKL-Elektriciteitskabel_E_Lv_Map_Cable_Noord_ligging.csv
     */
    def main (args: Array[String])
    {
        do_exit = !args.contains ("--unittest")

        // parser.parse returns Option[C]
        parser.parse (args, EnexisOptions ()) match
        {
            case Some (options) =>

                if (options.connect)
                {
                    if (options.verbose)
                        org.apache.log4j.LogManager.getLogger (getClass).setLevel (org.apache.log4j.Level.INFO)

                    val begin = System.nanoTime ()
                    val session = getSession (options)
                    session.sparkContext.setLogLevel (options.log_level.toString)
                    val version = session.version
                    log.info (s"Spark $version session established")
                    if (version.take (SPARK.length) != SPARK.take (version.length))
                    log.warn (s"Spark version ($version) does not match the version ($SPARK) used to build $APPLICATION_NAME")

                    val setup = System.nanoTime ()
                    log.info ("setup: " + (setup - begin) / 1e9 + " seconds")

                    val start = System.nanoTime
                    log.info ("attempting to create a topology")
                    val cimreader_options = new mutable.HashMap[String, String]()
                    cimreader_options.put ("path", options.output_file)
                    cimreader_options.put ("StorageLevel", "MEMORY_AND_DISK_SER")

                    val elements = session.sqlContext.read.format ("ch.ninecode.cim").options (cimreader_options).load (options.output_file).rdd.persist (StorageLevel.fromString (options.storage))
                    val before = elements.count
                    val topology = Topology (session: SparkSession, options: EnexisOptions)
                    val new_elements = topology.run ()
                    val after = new_elements.count
                    val export = new CIMExport (session)
                    export.export (new_elements, options.output_file, "Enexis")
                    log.info ("%s elements before, %s elements after (%s seconds)".format (before, after, (System.nanoTime - start) / 1e9))
                }
                else if (options.someFiles)
                {
                    if (options.verbose)
                        org.apache.log4j.LogManager.getLogger (getClass).setLevel (org.apache.log4j.Level.INFO)

                    val begin = System.nanoTime ()
                    val session = getSession (options)
                    session.sparkContext.setLogLevel (options.log_level.toString)
                    val version = session.version
                    log.info (s"Spark $version session established")
                    if (version.take (SPARK.length) != SPARK.take (version.length))
                        log.warn (s"Spark version ($version) does not match the version ($SPARK) used to build $APPLICATION_NAME")

                    val setup = System.nanoTime ()
                    log.info ("setup: " + (setup - begin) / 1e9 + " seconds")

                    val enexis = Enexis (session, options)
                    enexis.run ()

                    val calculate = System.nanoTime ()
                    log.info ("execution: " + (calculate - setup) / 1e9 + " seconds")
                }

                if (do_exit)
                    sys.exit (0)

            case None =>
                if (do_exit)
                    sys.exit (1)
        }
    }
}
