package ch.ninecode.enexis

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Encoders
import org.apache.spark.sql.Row
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import ch.ninecode.cim.CIMRDD
import ch.ninecode.model._

case class Topology (session: SparkSession, options: EnexisOptions) extends CIMRDD
{
    implicit val spark: SparkSession = session
    if (options.verbose)
        org.apache.log4j.LogManager.getLogger (getClass).setLevel (org.apache.log4j.Level.INFO)
    implicit val log: Logger = LoggerFactory.getLogger (getClass)

    // just to get a schema
    case class dummy (override val sup: Element = null) extends Element

    // long,lat concatenation
    type Position = String
    type Voltage = String
    type PSRType = String
    val factory: ElementFactory = ElementFactory ()

    def start_end (points: Iterable [PositionPoint]): List[Position] =
    {
        val start = points.minBy (_.sequenceNumber)
        val end = points.maxBy (_.sequenceNumber)
        List (start.xPosition.toString + "," + start.yPosition.toString, end.xPosition.toString + "," + end.yPosition.toString)
    }

    def one_and_two (position: List[Position], terminals: Iterable [Terminal]): List[(Position, Terminal)] =
    {
        val one = terminals.find (_.ACDCTerminal.sequenceNumber == 1)
        val two = terminals.find (_.ACDCTerminal.sequenceNumber == 2)
        List ((position.head, one.orNull), (position.tail.head, two.orNull))
    }

    def connect (pair: (Terminal, Terminal)): (ConnectivityNode, List[Terminal]) =
    {
        val parent1 = pair._1.ConductingEquipment
        val parent2 = pair._2.ConductingEquipment
        val parent = parent1 + "_" + parent2
        val node_obj = factory.identified_object ("_node_" + parent, null, "connectivity node for " + parent)
        val node = ConnectivityNode (node_obj, null, null, null)

        val obj1 = pair._1.ACDCTerminal.IdentifiedObject.copy ().asInstanceOf[IdentifiedObject]
        val terminal1 = factory.terminal (obj1, pair._1.ACDCTerminal.sequenceNumber, pair._1.ConductingEquipment, node.id)

        val obj2 = pair._2.ACDCTerminal.IdentifiedObject.copy ().asInstanceOf[IdentifiedObject]
        val terminal2 = factory.terminal (obj2, pair._2.ACDCTerminal.sequenceNumber, pair._2.ConductingEquipment, node.id)

        (node, terminal1 :: terminal2 :: Nil)
    }

    def update_nodes_terminals (connections: RDD[(ConnectivityNode, List[Terminal])]): Unit =
    {
        val nodes = connections.map (_._1)
        val terms = connections.flatMap (_._2)

        val keyedterms = terms.keyBy (_.id)
        val storage = StorageLevel.fromString (options.storage)

        val old_terminal = getOrElse[Terminal]
        old_terminal.name = "prior_Terminal"
        val new_terminal = old_terminal.keyBy (_.id).subtractByKey (keyedterms).values.union (terms)
        Terminal.subsetter.save (session.sqlContext, new_terminal.asInstanceOf[Terminal.subsetter.rddtype], storage)

        val old_acdcterminal = getOrElse[ACDCTerminal]
        old_acdcterminal.name = "prior_ACDCTerminal"
        val new_acdcterminal = old_acdcterminal.keyBy (_.id).subtractByKey (keyedterms).values.union (terms.map (_.ACDCTerminal))
        ACDCTerminal.subsetter.save (session.sqlContext, new_acdcterminal.asInstanceOf[ACDCTerminal.subsetter.rddtype], storage)

        val old_idobj = getOrElse[IdentifiedObject]
        old_idobj.name = "prior_IdentifiedObject"
        val new_idobj = old_idobj.keyBy (_.id).subtractByKey (keyedterms).values.union (terms.map (_.ACDCTerminal.IdentifiedObject)).union (nodes.map (_.IdentifiedObject))
        IdentifiedObject.subsetter.save (session.sqlContext, new_idobj.asInstanceOf[IdentifiedObject.subsetter.rddtype], storage)

        // swap the old Elements RDD for the new one
        val old_elements = getOrElse[Element]("Elements")
        val new_elements = old_elements.keyBy (_.id).subtractByKey (keyedterms).values.union (terms.asInstanceOf[RDD[Element]]).union (nodes.asInstanceOf[RDD[Element]])
        old_elements.name = "prior_Elements"
        new_elements.name = "Elements"
        new_elements.persist (storage)
        if (spark.sparkContext.getCheckpointDir.isDefined) new_elements.checkpoint ()
        val schema = Encoders.product[dummy].schema
        val data_frame = spark.sqlContext.createDataFrame (new_elements.asInstanceOf[RDD[Row]], schema)
        data_frame.createOrReplaceTempView ("Elements")
    }

    def connect_energy_consumers (): Unit =
    {
        log.info ("connect_energy_consumers")

        val terminals = getOrElse[Terminal].keyBy (_.ConductingEquipment)
        // get each consumer Terminal keyed by its long,lat position as a string
        val position_consumers: RDD[(Position, Terminal)] = getOrElse[EnergyConsumer].keyBy (_.EnergyConnection.ConductingEquipment.Equipment.PowerSystemResource.Location)
            .join (getOrElse[PositionPoint].keyBy (_.Location))
            .values.map (x ⇒ (x._2.xPosition.toString + "," + x._2.yPosition.toString, x._1))
            .keyBy (_._2.id).join (terminals).values
            .map (x ⇒ (x._1._1, x._2))
        // get each cable Terminal keyed by its long,lat start and end points as a string
        val position_cables: RDD[(Position, Terminal)] = get[ACLineSegment].keyBy (_.Conductor.ConductingEquipment.Equipment.PowerSystemResource.Location)
            .join (getOrElse[PositionPoint].keyBy (_.Location))
            .groupByKey.values
            .map (x ⇒ (x.head._1, start_end (x.map (y ⇒ y._2))))
            .keyBy (_._1.id).join (terminals)
            .groupByKey.values
            .flatMap (x ⇒ one_and_two (x.head._1._2, x.map (y ⇒ y._2)))
        // join by position to get matches between consumers and cable ends
        val pairs = position_consumers.join (position_cables).values
        val connections = pairs.map (connect)
        update_nodes_terminals (connections)
    }

    def connect_cables (): Unit =
    {
        log.info ("connect_cables")

        // get all PositionPoint grouped by Location and extract the start and end points
        val endpoints = getOrElse[PositionPoint].groupBy (_.Location).flatMap (
            pair ⇒
            {
                val location = pair._1
                val list = pair._2
                if (list.size > 1)
                {
                    val start: PositionPoint = list.minBy (_.sequenceNumber)
                    val end: PositionPoint = list.maxBy (_.sequenceNumber)
                    (location, (start, end)) :: Nil
                }
                else
                    Nil
            }
        )

        // get cables that have unconnected terminals and key by matching position point
        val raw_ends: RDD[(Position, (Terminal, String, String))] = getOrElse[ACLineSegment].map (x ⇒ (x.id, (
                x.Conductor.ConductingEquipment.Equipment.PowerSystemResource.Location,
                x.Conductor.ConductingEquipment.BaseVoltage,
                x.Conductor.ConductingEquipment.Equipment.PowerSystemResource.PSRType)))
            .join (getOrElse[Terminal].filter (x ⇒ x.ConnectivityNode == null).groupBy (_.ConductingEquipment))
            .values.keyBy (_._1._1)
            .join (endpoints)
            .values
            .flatMap (
                x ⇒
                {
                    // (((location, voltage, psrtype), Iterable[Terminal]), (start PositionPoint, end PositionPoint)
                    val voltage = x._1._1._2
                    val psrtype = x._1._1._3
                    val terminals = x._1._2
                    val start = x._2._1
                    val end = x._2._2
                    terminals.map (
                        terminal ⇒
                        {
                            val point = if (terminal.ACDCTerminal.sequenceNumber == 1) start else end
                            val key = point.xPosition.toString + "," + point.yPosition.toString
                            (key, (terminal, voltage, psrtype))
                        }
                    )
                }
            )

        def voltage_compatible (v1: String, v2: String): Boolean =
        {
            (v1 == v2) || (v1 == "BaseVoltage_Unknown" || v2 == "BaseVoltage_Unknown") || (v1 == "BaseVoltage_400" && v2 == "BaseVoltage_230") || (v1 == "BaseVoltage_230" && v2 == "BaseVoltage_400")
        }

        val connections = raw_ends.groupByKey.values.filter (_.size == 2).flatMap (
            pair ⇒
            {
                val t1 = pair.head._1
                val v1 = pair.head._2
                val p1 = pair.head._3
                val t2 = pair.tail.head._1
                val v2 = pair.tail.head._2
                val p2 = pair.tail.head._3
                if (voltage_compatible (v1, v2))
                {
                    if (p1 == p2)
                        List (connect ((t1, t2)))
                    else
                    {
                        log.warn ("cable %s and %s have different PSRType (%s & %s) and yet terminate at the same coordinates".format (t1.ConductingEquipment, t2.ConductingEquipment, p1, p2))
                        List()
                    }
                }
                else
                {
                    log.warn ("cable %s and %s are different voltage (%s & %s) and yet terminate at the same coordinates".format (t1.ConductingEquipment, t2.ConductingEquipment, v1, v2))
                    List()
                }
            }
        )

        update_nodes_terminals (connections)
    }

    def run (): RDD[Element] =
    {
        connect_energy_consumers ()
        connect_cables ()
        getOrElse[Element]("Elements")
    }
}
