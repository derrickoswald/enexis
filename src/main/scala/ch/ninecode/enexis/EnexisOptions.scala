package ch.ninecode.enexis

import ch.ninecode.enexis.Main.LogLevels
import ch.ninecode.enexis.Main.LogLevels.LogLevels

case class EnexisOptions
(
    /**
     * If <code>true</code>, don't call sys.exit().
     */
    unittest: Boolean = false,

    /**
     * If <code>true</code>, emit progress messages.
     */
    verbose: Boolean = false,

    /**
     * Spark master.
     */
    master: String = "local[*]",

    /**
     * Spark options.
     */
    options: Map[String, String] = Map (),

    /**
     * Storage level for RDD serialization.
     */
    storage: String = "MEMORY_AND_DISK_SER",

    /**
     * Logging level.
     */
    log_level: LogLevels = LogLevels.OFF,

    /**
     * If <code>true</code>, apply spatial filter to reduce data volume.
     */
    filter: Boolean = false,

    /**
     * If <code>true</code>, attempt to connect elements into a topology.
     */
    connect: Boolean = false,

    /**
     * Name of target file
     */
    output_file: String = "enexis.rdf",

    /**
     * Equipment libraries.
     */
    libraries: Seq[String] = Seq (),

    /**
     * Source CSV files of cable data.
     */
    cable_files: Seq[String] = Seq (),

    /**
     * Source CSV files of service connection data.
     */
    service_connection_files: Seq[String] = Seq (),

    /**
     * Source CSV files of multi-story connection data.
     */
    multistory_connection_files: Seq[String] = Seq (),

    /**
     * Source CSV files of charging point data.
     */
    charging_point_files: Seq[String] = Seq (),

    /**
     * Source CSV files of street furniture data.
     */
    street_furniture_files: Seq[String] = Seq (),

    /**
     * Source CSV files of street light data.
     */
    street_light_files: Seq[String] = Seq (),

    /**
     * Source CSV files of station data.
     */
    station_files: Seq[String] = Seq (),

    /**
     * Source CSV files of distribution box data.
     */
    distribution_files: Seq[String] = Seq (),

    /**
     * Source CSV files of junction box data.
     */
    junction_files: Seq[String] = Seq ()
)
{
    def file_count: Int =
        cable_files.size +
        service_connection_files.size +
        multistory_connection_files.size +
        charging_point_files.size +
        street_furniture_files.size +
        street_light_files.size +
        station_files.size +
        distribution_files.size +
        junction_files.size

    def someFiles: Boolean =
        cable_files.nonEmpty ||
        service_connection_files.nonEmpty ||
        multistory_connection_files.nonEmpty ||
        charging_point_files.nonEmpty ||
        street_furniture_files.nonEmpty ||
        street_light_files.nonEmpty ||
        station_files.nonEmpty ||
        distribution_files.nonEmpty ||
        junction_files.nonEmpty
}