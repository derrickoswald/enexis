package ch.ninecode.enexis

import scala.reflect.runtime.universe

import ch.ninecode.cim.Parseable
import ch.ninecode.model._

case class ElementFactory ()
{
    private lazy val universeMirror = universe.runtimeMirror(getClass.getClassLoader)

    def companionOf[T <: Element] (implicit tag: universe.TypeTag[T]): Parseable[T] =
    {
        val companionMirror = universeMirror.reflectModule (universe.typeOf[T].typeSymbol.companion.asModule)
        companionMirror.instance.asInstanceOf[Parseable[T]]
    }

    type Bit = Int
    def bitflag (s: String): Bit = if (null == s || "" == s) 0 else 1

    def setFields[T <: Element] (items: Iterable[(String, Bit)]) (implicit tag: universe.TypeTag[T]) : Array[Int] =
    {
        val fields = companionOf[T].fields
        val shifts = items.map (item => (fields.indexOf (item._1), item._2))
        val bitfields = Array(0, 0, 0)
        shifts.foreach (
            shift =>
            {
                val (position, bit) = shift;
                val index = position / 32;
                bitfields(index) = bitfields(index) | (bit << (position % 32))
            }
        )
        bitfields.reverse.dropWhile (_ == 0).reverse
    }

    def setFields[T <: Element] (items: Array[String]) (implicit tag: universe.TypeTag[T]) : Array[Int] =
    {
        setFields[T] (items.map (item => (item, 1)))
    }

    def setFields[T <: Element] (item: String) (implicit tag: universe.TypeTag[T]) : Array[Int] =
    {
        setFields[T] (Array (item))
    }

    val three = "http://iec.ch/TC57/2013/CIM-schema-cim16#PhaseCode.ABC"

    def basic (mRID: String): BasicElement =
    {
        val element = BasicElement (sup = null, mRID = mRID)
        element.bitfields = Array (1)
        element
    }

    def identified_object (mRID: String, name: String = null, description: String = null, alias: String = null): IdentifiedObject =
    {
        val element = basic (mRID)
        val obj = IdentifiedObject (sup = element, aliasName = alias, description = description, mRID = mRID, name = name)
        obj.bitfields = setFields[IdentifiedObject] (
            ("aliasName", bitflag (alias)) :: ("description", bitflag (description)) :: ("mRID", 1) :: ("name", bitflag (name)) :: Nil)
        obj
    }

    def terminal (obj: IdentifiedObject, sequence: Int, parent: String, node: String = null): Terminal =
    {
        val acdc = ACDCTerminal (sup = obj, sequenceNumber = sequence)
        acdc.bitfields = setFields[ACDCTerminal] ("sequenceNumber")
        val terminal = Terminal (sup = acdc, phases = three, ConductingEquipment = parent, ConnectivityNode = node)
        terminal.bitfields = setFields[Terminal] (("phases", 1) :: ("ConductingEquipment", 1) :: ("ConnectivityNode", bitflag (node)) :: Nil)
        terminal
    }
}
