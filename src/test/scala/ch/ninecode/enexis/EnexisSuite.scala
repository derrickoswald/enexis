package ch.ninecode.enexis

import java.io.BufferedOutputStream
import java.io.Closeable
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.util
import java.util.zip.ZipInputStream

import scala.collection.JavaConverters._

import org.scalatest.BeforeAndAfterAll
import org.scalatest.FunSuite

import ch.ninecode.enexis.Main.main

class EnexisSuite extends FunSuite with BeforeAndAfterAll
{
    /**
     * This utility extracts files and directories of a standard zip file to
     * a destination directory.
     *
     * @author www.codejava.net
     *
     */
    class Unzip
    {
        /**
         * Extracts a zip file specified by the file to a directory.
         *
         * The directory will be created if does not exist.
         *
         * @param file      The Zip file.
         * @param directory The directory to extract it to
         * @throws IOException If there is a problem with the zip extraction
         */
        @throws[IOException]
        def unzip (file: String, directory: String): Unit =
        {
            val dir = new File (directory)
            if (!dir.exists)
                dir.mkdir
            val zip = new ZipInputStream (new FileInputStream (file))
            var entry = zip.getNextEntry
            // iterates over entries in the zip file
            while (null != entry)
            {
                val path = directory + entry.getName
                if (!entry.isDirectory)
                // if the entry is a file, extracts it
                    extractFile (zip, path)
                else
                // if the entry is a directory, make the directory
                    new File (path).mkdir
                zip.closeEntry ()
                entry = zip.getNextEntry
            }
            zip.close ()
        }

        /**
         * Extracts a zip entry (file entry).
         *
         * @param zip  The Zip input stream for the file.
         * @param path The path to extract he file to.
         * @throws IOException If there is a problem with the zip extraction
         */
        @throws[IOException]
        private def extractFile (zip: ZipInputStream, path: String): Unit =
        {
            val bos = new BufferedOutputStream (new FileOutputStream (path))
            val bytesIn = new Array[Byte](4096)
            var read = -1
            while (
            {
                read = zip.read (bytesIn)
                read != -1
            })
                bos.write (bytesIn, 0, read)
            bos.close ()
        }
    }

    def using[T <: Closeable, R] (resource: T)(block: T => R): R =
    {
        try
        {
            block (resource)
        }
        finally
        {
            resource.close ()
        }
    }

    override def beforeAll ()
    {
        if (!new File ("data/test_station.csv").exists)
            new Unzip ().unzip ("data/test_station.zip", "data/")
        if (!new File ("data/test_service_connection.csv").exists)
            new Unzip ().unzip ("data/test_service_connection.zip", "data/")
        if (!new File ("data/test_cable.csv").exists)
            new Unzip ().unzip ("data/test_cable.zip", "data/")
        if (!new File ("data/equipmentlibrary.rdf").exists)
            new Unzip ().unzip ("data/equipmentlibrary.zip", "data/")
        if (!new File ("data/header.rdf").exists)
            new Unzip ().unzip ("data/header.zip", "data/")
    }

    override def afterAll (): Unit =
    {
        new File ("data/test_station.csv").delete
        new File ("data/test_service_connection.csv").delete
        new File ("data/test_cable.csv").delete
        new File ("data/equipmentlibrary.rdf").delete
        new File ("data/header.rdf").delete
    }

    test ("Help")
    {
        main (Array ("--unittest", "--help"))
    }

    test ("Cable")
    {
        val args = Array (
            "--unittest",
            "--verbose",
            "--master", "local[*]",
            "--output", "data/test_cable.rdf",
            "--library", "data/equipmentlibrary.rdf",
            "--library", "data/header.rdf",
            "--cable", "data/test_cable.csv")
        main (args)
    }

    test ("ServiceConnection")
    {
        val args = Array (
            "--unittest",
            "--verbose",
            "--master", "local[*]",
            "--output", "data/test_service_connection.rdf",
            "--library", "data/equipmentlibrary.rdf",
            "--library", "data/header.rdf",
            "--service", "data/test_service_connection.csv")
        main (args)
    }

    test ("Station")
    {
        val args = Array (
            "--unittest",
            "--verbose",
            "--master", "local[*]",
            "--output", "data/test_station.rdf",
            "--library", "data/equipmentlibrary.rdf",
            "--library", "data/header.rdf",
            "--station", "data/test_station.csv")
        main (args)
    }

//    test ("Connectivity")
//    {
//        val args = Array (
//            "--unittest",
//            "--verbose",
//            "--master", "local[*]",
//            "--connect",
//            "--output", "data/test_connectivity.rdf",
//            "--library", "data/equipmentlibrary.rdf",
//            "--library", "data/header.rdf",
//            "--cable", "data/test_connectivity.csv",
//            "--service", "data/test_service_connection.csv")
//        main (args)
//    }
}
